using System;
using Xunit;

namespace SampleWebTest
{
    public class SampleTests
    {
        [Fact]
        public void testSample()
        {
            var n = 1;
            Assert.Equal(1, n);
        }
    }
}
